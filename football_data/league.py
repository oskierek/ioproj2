import matplotlib.pyplot as plt


from .db_interface import DbInterface

db_filename = 'football_data/database.sqlite'

class LeagueParser:
    def __init__(self, league_name, bookies):
        db = DbInterface(db_filename)
        self.league_name = league_name
        self.league_df = db.getLeagueMatchesData(league_name)

        df = self.league_df
        df['season'] = df['season'].astype(int)
        df['stage'] = df['stage'].astype(int)
        df.drop(bookies, axis=1, inplace=True)
        df.drop('date', axis=1, inplace=True)
        df['stage'] = df.apply(lambda row: row.stage + 34 * row.season, axis=1)
        df.sort_values('stage', ascending=True, inplace=True)
        df.dropna(axis=0, inplace=True)
        player_column_labels = list()
        for i in range(11):
            player_column_labels.append('home_player_{}'.format(i+1))
        for i in range(11):
            player_column_labels.append('away_player_{}'.format(i+1))
        for label in player_column_labels:
            df[label] = df[label].astype(int)
        self.sorted_df = df


    def plotSeason(self, season_no):
        self.league_df = self.league_df[self.league_df['season'] == season_no]
        self.league_df['stage'] = self.league_df['stage'].astype(int)
        self.league_df = self.league_df[['stage', 'home_team_api_id', 'away_team_api_id', 'match_result']]
        self.league_df = self.league_df.sort_values('stage', ascending=True)

        season_data = self.computeSeasonTable()

        self.plotSeasonsData(season_data, season_data.keys())

        return season_data


    def plotSeasonsData(self, data, keys, start_stage=0, stop_stage=272):
        cnt=0

        # matplotlib has just 8 colors y default
        for key in keys:
            if cnt < 8:
                shape='o'
            elif cnt < 16:
                shape='^'
            elif cnt < 24:
                shape='v'
            else:
                shape='s'

            plt.plot(list(range(len(data[key][0]))), data[key][0],
                    label=data[key][2])
            cnt+=1

        plt.title('{} season {}'.format(self.league_name.capitalize(), 'all-seasons'))
        plt.legend()
        #plt.xlim(start_stage, stop_stage)
        plt.grid(which='both')
        plt.show()


    def computeAllTimeSeries(self):
        #self.league_df = self.league_df[self.league_df['season'] == season_no]
        self.sorted_df['stage'] = self.sorted_df['stage'].astype(int)
        self.sorted_df = self.sorted_df[['stage', 'home_team_api_id', 'away_team_api_id', 'match_result']]
        self.sorted_df = self.sorted_df.sort_values('stage', ascending=True)#

        all_time_table = self.computeSeasonTable(self.sorted_df, 1, -1, 0)

        return all_time_table


    def computeSeasonTable(self, df, win_points=3, defeat_points=0, draw_points=1):
        team_ids = df.home_team_api_id.unique()
        team_ids = [str(i) for i in team_ids]

        db = DbInterface(db_filename)
        # names etc
        team_details = db.getTeamsDetails(team_ids)

        season_table = dict()
        for key in team_ids:
            season_table[key] = [0]

        for match in df.values:
            home = str(match[1])
            away = str(match[2])
            result = str(match[3])
            if result == 'D':
                # both get 1 point by default
                points = season_table[home][-1]
                points += draw_points
                season_table[home].append(points)
                points = season_table[away][-1]
                points += draw_points
                season_table[away].append(points)
            elif result == 'H':
                # home team won and get 3 pts by default
                points = season_table[home][-1]
                points += win_points
                season_table[home].append(points)
                season_table[away].append(season_table[away][-1] + defeat_points)
            else:
                # away team wins
                points = season_table[away][-1]
                points += win_points
                season_table[away].append(points)
                season_table[home].append(season_table[home][-1] + defeat_points)

        for key in season_table.keys():
            for team in team_details:
                if int(key) in team:
                    #print(season_table[key])
                    season_table[key] = (season_table[key], team[3], team[4])

        return season_table

