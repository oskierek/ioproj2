import sqlite3
import pandas as pd

season_replacement = {
    '2008/2009':0, '2009/2010':1, '2010/2011':2, '2011/2012':3,
    '2012/2013':4, '2013/2014':5, '2014/2015':6, '2015/2016':7,
}

league_ids = {
    "bundesliga" : 7809,
    "premier-league" : 1729,
    "ektraklasa" : 15722,
}

def determineWinner(frame):
    home = int(frame['home_team_goal'])
    away = int(frame['away_team_goal'])
    if home == away:
        return 'D'
    elif home > away:
        return 'H'
    else:
        return 'A'

class DbInterface:
    conn = ''

    def __init__(self, db_file):
        self.conn = sqlite3.connect(db_file)


    def __del__(self):
        try:
            self.conn.close()
            self.cursor.close()
        except sqlite3.ProgrammingError:
            # already closed
            pass
        print('bb')


    def showTables(self):
        self.cursor = self.conn.cursor()
        self.cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")

        tables = self.cursor.fetchall()
        for table in tables[1:]:
            print(table[0])

        self.cursor.close()


    def getLeagueMatchesData(self, league_key, drop_formation=True, drop_missing_players=False):
        league_id = league_ids[league_key]
        self.cursor = self.conn.cursor()
        league_table = pd.read_sql_query("SELECT * from Match where league_id={}".format(league_id), self.conn)
        self.cursor.close()
        league_table.drop(['id', 'country_id', 'league_id', 'match_api_id'], axis=1, inplace=True)
        league_table = league_table.drop('goal,shoton,shotoff,foulcommit,card,cross,corner,possession'.split(','), axis=1)

        if drop_formation:
            # lets drop formation for now
            coordinates_drop = list()
            player_columns_home = list()
            player_columns_away = list()
            for i in range(11):
                player_columns_home.append('home_player_{}'.format(i+1))
                player_columns_away.append('away_player_{}'.format(i+1))
                coordinates_drop.append('home_player_X{}'.format(i+1))
                coordinates_drop.append('away_player_X{}'.format(i+1))
                coordinates_drop.append('home_player_Y{}'.format(i+1))
                coordinates_drop.append('away_player_Y{}'.format(i+1))
            league_table.drop(coordinates_drop, axis=1, inplace=True)
        # Dropping some of earliest season match entries because some players are missing
        # in different teams so assuming so early in the history it does not matter
        # for more current seasons
        # could just get rid of 1st season
        if drop_missing_players:
            league_table = league_table.dropna(subset=player_columns_home)
            league_table = league_table.dropna(subset=player_columns_away)

            league_table[player_columns_home] = league_table[player_columns_home].astype(int)
            league_table[player_columns_away] = league_table[player_columns_away].astype(int)
        league_table['season'].replace(season_replacement, inplace=True)

        league_table['match_result'] = league_table.apply(determineWinner, axis=1)
        tmp = list(league_table.columns)
        tmp.insert(5, tmp[-1])
        tmp = tmp[:-1]
        league_table = league_table[tmp]

        return league_table


    def getTeamsDetails(self, team_ids):
        self.cursor = self.conn.cursor()
        team_ids = [str(i) for i in team_ids]
        self.cursor.execute("SELECT * from Team where team_api_id in ({});".format(','.join(list(team_ids))))
        teams_details = self.cursor.fetchall()
        self.cursor.close()

        return teams_details


    def getTeamAttributes(self):
        pass


    def getPlayerAttributes(self, player_ids):
        ret = dict()
        self.cursor = self.conn.cursor()
        for player_id in player_ids:
            self.cursor.execute("SELECT avg(overall_rating) FROM player_attributes WHERE player_api_id={}".format(player_id))
            player_details = self.cursor.fetchall()
            ret[player_id] = player_details[0][0]

        self.cursor.close()
        # can be expanded to remaining stats

        return ret


    def allToCsv(self, output_dir=''):
        self.cursor = self.conn.cursor()
        self.cursor.execute("SELECT name from sqlite_master where type='table';")
        tables = self.cursor.fetchall()
        for table_name in tables:
            table_name = table_name[0]
            table = pd.read_sql_query("SELECT * from {}".format(table_name), self.conn)
            table.to_csv('{}{}.csv'.format(output_dir, table_name.lower()), index_label='index')

        self.cursor.close()
