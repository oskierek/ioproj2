from statsmodels.tsa.arima.model import ARIMA
import matplotlib.pyplot as plt

class MyArima:
    def __init__(self, data):
        self.data = data


    def prepareAndFitModel(self, idx, pdq=(5,1,0)):
        keys = list(self.data.keys())
        print(self.data[keys[idx]][0])
        self.model = ARIMA(self.data[keys[idx]][0], order=pdq)
        self.model_fit = self.model.fit()


    def predictAndValidate(self, key, club, pdq=(34,1,1), train_ratio=0.7, debug_log=False, plot_result=True):
        arima_data = self.data[key][0]
        size = len(arima_data)
        comp = 0
        # if train_ratio = 1 -> sim latest season
        if train_ratio < 0:
            size *= train_ratio
        else:
            size -= 34
        size = int(size)
        train, test = arima_data[0:size], arima_data[size:]
        history = [x for x in train]
        predictions = list()
        derivative_history = [0]
        derivative_predictions = [0]
        # walk forward validation
        for t in range(len(test)):
            model = ARIMA(history, order=pdq)
            model_fit = model.fit()
            output = model_fit.forecast()
            predicted_result = int(0.5 + output[0])

            # compensate just for plots to start at the same point
            if t==0:
                #comp = predicted_result-test[t]
                pass
            else:
                if predicted_result - predictions[-1] > 1:
                    predicted_result = predictions[-1] + 1
                elif predicted_result - predictions[-1] < -1:
                    predicted_result = predictions[-1] -1
                derivative_predictions.append(predicted_result - predictions[-1])
                derivative_history.append(test[t] - history[-1])
            predictions.append(predicted_result)
            history.append(test[t])

            if debug_log:
                print('Predicted: {}, expected: {}'.format(predicted_result, test[t]))

        if plot_result:
            plt.title('Test samples = {}, p={}'.format(len(test), pdq[0]))
            plt.plot([x + comp for x in test], '-b^', label='{}-test'.format(club))
            plt.plot(predictions, 'ro', label='{}-predict'.format(club))
            plt.legend()
            plt.show()
            plt.title('Derivative')
            plt.plot(derivative_history, '-b^', label='{}-test'.format(club))
            plt.plot(derivative_predictions, 'ro', label='{}-predict'.format(club))
            plt.legend()
            plt.show()

        return predictions