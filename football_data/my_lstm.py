from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
import numpy as np
import matplotlib.pyplot as plt


# convert an array of values into a dataset matrix
def fun(data, look_back=1):
	X, Y = [], []
	for i in range(len(data)-look_back-1):
		a = data[i:(i+look_back), 0]
		X.append(a)
		Y.append(data[i + look_back, 0])
	return np.array(X), np.array(Y)


class MyLstm:
    def __init__(self, data):
        self.data=data


    def predict(self, key, look_back=1, plot_result=True):
        data = self.data[key][0]
        data = [float(d) for d in data]
        scaler = MinMaxScaler(feature_range=(0,1))
        data = np.array(data)
        data = data.reshape(-1,1)
        data = scaler.fit_transform(data)

        # split into train and test sets
        train_size = int(len(data) * 0.67)
        train, test = data[0:train_size,:], data[train_size:len(data),:]

        trainX, trainY = fun(train, look_back)
        testX, testY = fun(test, look_back)

        trainX = np.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
        testX = np.reshape(testX, (testX.shape[0], 1, testX.shape[1]))

        model = Sequential()
        model.add(LSTM(4, input_shape=(1, look_back)))
        model.add(Dense(1))
        model.compile(loss='mean_squared_error', optimizer='adam')
        model.fit(trainX, trainY, epochs=100, batch_size=10, verbose=2)

        # make predictions
        trainPredict = model.predict(trainX)
        testPredict = model.predict(testX)
        # invert predictions
        trainPredict = scaler.inverse_transform(trainPredict)
        trainY = scaler.inverse_transform([trainY])
        testPredict = scaler.inverse_transform(testPredict)
        testY = scaler.inverse_transform([testY])

        n = len(testY[0])
        trainY = trainY[0][-n:]
        testY = testY[0]


        if plot_result:
            plt.plot(trainY, label='{}-train'.format(self.data[key][2]))
            plt.plot(testY, '.', label='{}-test'.format(self.data[key][2]))
            plt.legend()
            plt.show()

        cnt=0
        der_cnt=0
        derTrain, derTest = [], []
        for i in range(1, n):
            derTest.append(testY[i] - testY[i-1])
            derTrain.append(trainY[i] - trainY[i-1])
            if trainY[i] - trainY[i-1] == testY[i] - testY[i-1]:
                cnt+=1

        if plot_result:
            plt.plot(derTrain, label='{}-train'.format(self.data[key][2]))
            plt.plot(derTest, 'ro', label='{}-test'.format(self.data[key][2]))
            plt.legend()
            plt.show()

        print((100*cnt)/n)
        return (100*cnt)/n

