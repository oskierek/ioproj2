from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
from sklearn.naive_bayes import GaussianNB

from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
from sklearn.preprocessing import OneHotEncoder
import pandas as pd

class MatchAI:
    def prepare_data(self, train_ratio, new_df):
        if new_df==False:
            labels = self.df.columns
            X = self.df[labels[:-1]]
            y = self.df[labels[-1]]
        else:
            labels = self.new_df.columns
            X = self.new_df[labels[:-1]]
            y = self.new_df[labels[-1]]

        return train_test_split(X, y, train_size=train_ratio)

    def calculate_result(self, y_pred, y_test):
        y_test = y_test.tolist()
        y_pred = y_pred.tolist()
        cnt=0

        for i in range(len(y_pred)):
            if y_pred[i] == y_test[i]:
                cnt += 1

        return cnt

    def __init__(self, df):
        self.df = df
        result_mapping = {'D':0, 'H':1, 'A':2}
        self.df['match_result'].replace(result_mapping, inplace=True)
        self.df['stage'] = self.df['stage'].astype(int)
        self.df['home_team_api_id'] = self.df['home_team_api_id'].astype(int)
        self.df['away_team_api_id'] = self.df['away_team_api_id'].astype(int)

        new_cols = list()
        home_players = list()
        away_players = list()
        for i in range(11):
            new_cols.append('player_{}'.format(i+1))
            home_players.append('home_player_{}'.format(i+1))
            away_players.append('away_player_{}'.format(i+1))
        home_players.append('match_result')
        away_players.append('match_result')
        new_cols.append('match_result')

        # 0 - draw, 1 - won, 2-lost
        from_home_players = self.df[home_players]
        from_home_players['match_result'].replace({0:0, 1:1, 2:2}, inplace=True)
        from_home_players.columns = new_cols
        from_away_players = self.df[away_players]
        from_away_players['match_result'].replace({0:0, 1:2, 2:1}, inplace=True)
        from_away_players.columns = new_cols
        new_df = pd.concat([from_home_players, from_away_players])
        self.new_df = new_df


    def knn(self, neighbours = 5, train_ratio=0.66, new_df=False):
        X_train, X_test, y_train, y_test = self.prepare_data(train_ratio, new_df)

        knn = KNeighborsClassifier(n_neighbors=neighbours, metric='euclidean')
        knn.fit(X_train, y_train)
        y_pred = knn.predict(X_test)

        cnt = self.calculate_result(y_pred, y_test)

        return (cnt * 100) / len(y_pred)


    def bayes(self, train_ratio=0.66, new_df=False):
        X_train, X_test, y_train, y_test = self.prepare_data(train_ratio, new_df)
        gnb = GaussianNB()
        y_pred = gnb.fit(X_train, y_train).predict(X_test)

        cnt = self.calculate_result(y_pred, y_test)

        return (cnt*100) / len(y_pred)


    def neuralnetwork(self, train_ratio=0.66, new_df=False):
        if new_df:
            verbose=0
            l1=len(self.new_df)
            l2 = l1 // 10
            input_size=11
            df = self.new_df
        else:
            verbose=0
            l1=1000
            l2=100
            input_size=22
            df = self.df
            df = df.drop(['index', 'home_team_api_id', 'away_team_api_id', 'stage'], axis=1)
        cols = df.columns
        X = df[cols[:-1]].to_numpy()
        y = df[cols[-1]].to_numpy()
        y = y.reshape(-1, 1)
        encoder=OneHotEncoder(sparse=False)
        y = encoder.fit_transform(y)
        train_x, test_x, train_y, test_y = train_test_split(X, y, test_size=0.33)
        model = Sequential()
        model.add(Dense(l1, input_shape=(input_size,), activation='relu', name='fc1'))
        model.add(Dense(200, activation='relu', name='fc2'))
        model.add(Dense(3, activation='softmax', name='output'))
        optimizer = Adam(lr=0.001)
        model.compile(optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

        model.fit(train_x, train_y, verbose=verbose, batch_size=50, epochs=100)

        self.results = model.evaluate(test_x, test_y)

        return self.results[1] * 100
