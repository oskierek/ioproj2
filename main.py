import config as cfg

import pandas as pd
import numpy as np
from matplotlib import pyplot

from football_data.league import LeagueParser
from football_data.time_series_prediction import MyArima
from football_data.db_interface import DbInterface
from football_data.match_ai import MatchAI
from football_data.my_lstm import MyLstm

if __name__ == "__main__":
    # get the football data
    league_key = 'bundesliga'

    lp = LeagueParser(league_key, cfg.bookies)

    data = lp.computeAllTimeSeries()
    # lp.plotSeasonsData(data, ['9823'])
    # lp.plotSeasonsData(data, ['9788'])
    # lp.plotSeasonsData(data, ['8350'])
    # lp.plotSeasonsData(data, ['8357'])

    keys = list(data.keys())

    if cfg.run_lstm:
        lstm = MyLstm(data)
        lstm_results = dict()

        for i in range(10):
            for key in keys:
                lb = int(0.05 * len(data[key][0]))
                #lstm_results[key] = (lstm.predict(key, look_back=lb, plot_result=False), len(data[key][0]))
                try:
                    tmp = lstm_results[key][0]
                    lstm_results[key] = ((tmp + lstm.predict(key, look_back=lb, plot_result=False))/2, len(data[key][0]))
                except KeyError:
                    lstm_results[key] = (lstm.predict(key, look_back=lb, plot_result=False), len(data[key][0]))

        print(lstm_results)

    if cfg.run_arima:
        arima = MyArima(data)
        # HSV
        arima.predictAndValidate('9790', club=data['9790'][2],debug_log=True)
        # Leverkusen
        # arima.predictAndValidate('9790', club=data['9790'][2],debug_log=True)

    # unique_players_set = set()

    # player_column_labels = list()
    # for i in range(11):
    #     player_column_labels.append('home_player_{}'.format(i+1))
    # for i in range(11):
    #     player_column_labels.append('away_player_{}'.format(i+1))

    # del lp

    # create_players_df = False
    # if create_players_df:
    #     lp = LeagueParser(league_key)
    #     df = lp.league_df
    #     df['season'] = df['season'].astype(int)
    #     df['stage'] = df['stage'].astype(int)
    #     df.drop(cfg.bookies, axis=1, inplace=True)
    #     df.drop('date', axis=1, inplace=True)
    #     df['stage'] = df.apply(lambda row: row.stage + cfg.bundesliga_matches_in_season * row.season, axis=1)
    #     df.sort_values('stage', ascending=True, inplace=True)
    #     df.dropna(axis=0, inplace=True)
    #     for label in player_column_labels:
    #         df[label] = df[label].astype(int)

    #     for label in player_column_labels:
    #         tmp = df[label].unique()
    #         for i in tmp:
    #             unique_players_set.add(i)

    #     df.drop(['season', 'home_team_goal', 'away_team_goal'], axis=1, inplace=True)

    #     tmp_cols = df.columns.tolist()
    #     tmp = tmp_cols.index('match_result')
    #     tmp_cols = tmp_cols[0:tmp] + tmp_cols[tmp+1:]
    #     tmp_cols.append('match_result')
    #     df = df[tmp_cols]

    #     db = DbInterface('football_data/database.sqlite')

    #     # da sie to za jednym query?
    #     # contains average overall of the player
    #     unique_players  = db.getPlayerAttributes(unique_players_set)
    #     df.replace(unique_players, inplace=True)

    #     df.to_csv('{}/{}/league_matches_{}.csv'.format('football_data/csv/', league_key, league_key), index_label='index')

    df = pd.read_csv('{}/{}/league_matches_{}.csv'.format('football_data/csv/', league_key, league_key))

    mai = MatchAI(df)

    knn_res5 = 0
    knn_res7 = 0
    knn_res11 = 0
    bayes = 0
    nn = 0

    for i in range(10):
        knn_res5 = mai.knn()
        knn_res7 = mai.knn(neighbours=7)
        knn_res11 = mai.knn(neighbours=11)
        bayes = mai.bayes()
        nn = mai.neuralnetwork()

    res_matches = [knn_res5, knn_res7, knn_res11, bayes, nn]

    knn_res5 = 0
    knn_res7 = 0
    knn_res11 = 0
    bayes = 0
    nn = 0

    for i in range(10):
        knn_res5 = mai.knn()
        knn_res7 = mai.knn(neighbours=7)
        knn_res11 = mai.knn(neighbours=11)
        bayes = mai.bayes()
        nn = mai.neuralnetwork()

    res_teams = [knn_res5, knn_res7, knn_res11, bayes, nn]

    # print('knn, n=5: {:.2f} %'.format(mai.knn()))
    # print('knn, n=7: {:.2f} %'.format(mai.knn(neighbours=7)))
    # print('knn, n=11: {:.2f} %'.format(mai.knn(neighbours=11)))
    # print('Naive Bayes: {:.2f} %'.format(mai.bayes()))
    # print('NN: {:.2f} %'.format(mai.neuralnetwork()))

    # print('knn, n=5: {:.2f} %'.format(mai.knn(new_df=True)))
    # print('knn, n=7: {:.2f} %'.format(mai.knn(neighbours=7, new_df=True)))
    # print('knn, n=11: {:.2f} %'.format(mai.knn(neighbours=11, new_df=True)))
    # print('Naive Bayes: {:.2f} %'.format(mai.bayes(new_df=True)))
    # print('NN: {:.2f} %'.format(mai.neuralnetwork(new_df=True)))

    print(res_matches)
    print(res_teams)
