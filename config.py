db_file = 'football_data/database.sqlite'
csv_out_data_dir = 'football_data/csv/'

league_ids = {
    "bundesliga" : 7809,
    "premier-league" : 1729,
    "ektraklasa" : 15722,
}

bookies = 'B365H,B365D,B365A,BWH,BWD,BWA,IWH,IWD,IWA,LBH,LBD,LBA,PSH,PSD,PSA,WHH,WHD,WHA,SJH,SJD,SJA,VCH,VCD,VCA,GBH,GBD,GBA,BSH,BSD,BSA'.split(',')

bundesliga_matches_in_season = 34

run_lstm = False
run_arima = False

